# **MIKL.io**

<table>
  <thead>
    <tr>
      <th align="left">Automated deployment of personal applications</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td valign="top">– Containerized applications and database</td>
    </tr>
    <tr>
      <td valign="top">– Automated continuous deployment via GitHub Actions</td>
    </tr>
    <tr>
      <td valign="top">– Vertically scaled hosting in AWS</td>
    </tr>
    <tr>
      <td valign="top">– Dependently networked by Docker Compose</td>
    </tr>
  </tbody>
</table>
